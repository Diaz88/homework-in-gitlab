# Напишите приммеры использования всех операций со словарями
# Оберните все операции в функции, которые принимают словарь и выполняют над ним операцию. Функцию надо вызвать.
country = {
    'USA' : 'Washington',
    'England' : 'London',
    'France' : 'Paris',
    'Russia' : 'Moscow'
}
country.clear()
def dict(strany):
    print(strany)
dict(country)

country = {
    'USA' : 'Washington',
    'England' : 'London',
    'France' : 'Paris',
    'Russia' : 'Moscow'
}
country_1 = country.copy()
def dict(strany):
    print(strany)
dict(country)
dict(country_1)


x = country.fromkeys(['England','France','Germany'], 'Europe', )
def strany(y):
    print(y)
strany(x)

country = {
    'USA' : 'Washington',
    'England' : 'London',
    'France' : 'Paris',

}
print(*country)
x = country.get('the developed','countries!')
def strany(y):
    print('the developed', y)
strany(x)


country = {
    'USA' : 'Washington',
    'England' : 'London',
    'France' : 'Paris',
    'Russia' : 'Moscow'
}
print(country.pop('USA'))
def strany(x):
    print(x)
strany(country)


country = {
    'USA' : 'Washington',
    'England' : 'London',
    'France' : 'Paris',
    'Russia' : 'Moscow'
}
print(country.popitem())
def strany(x):
    print(x)
strany(country)



country = {
    'USA' : 'Washington',
    'England' : 'London',
    'France' : 'Paris',
    'Russia' : 'Moscow'
}
print(country.items())
def strany(x):
    print(x)
strany(country)


country = {
    'USA' : 'Washington',
    'England' : 'London',
    'France' : 'Paris',
    'Russia' : 'Moscow'
}
print(country.keys())
def strany(x):
    print(x)
strany(country)


country = {
    'USA' : 'Washington',
    'England' : 'London',
    'France' : 'Paris',
    'Russia' : 'Moscow'
}
print(country.values())
def strany(x):
    print(x)
strany(country)



# Задача для гугления и самостоятельной рабооты. Разобрраться как работает метод dict.update() и dict.setdefault()
country = {
    'USA' : 'Washington',
    'England' : 'London',
    'France' : 'Paris',
    'Russia' : 'Moscow'
}
country.setdefault('Kyrgyzstan', 'Bishkek')
print(country)


country = {
    'USA' : 'Washington',
    'England' : 'London',
    'France' : 'Paris',
    'Russia' : 'Moscow'
}
country.update(Russia = 'Nasha', Rodnoi = 'Bishkek')
print(country)



# Напишите пример вложенной функции.
def outer_func():
    def inner_func():
        print("Hello, World!")

    inner_func()
outer_func()




# Напишите функцию принимающую массив (состоящий из слов, название файла) и при помощи данных аргументов создающую
# - запись в файле
array = ['new_work', 'homework']
def func(x):
    print(x)
mass = open('new_work', 'w' )
mass.write('Hello my friends,  ')
mass.close()

func(array)
# Напишите функцию принимающую массив (состоящий из слов, название файла) и при помощи данных аргументов дополняющую
# - файл новыми записями
array = ['new_work', 'homework']
def func(x):
    print(x)

mass = open('new_work', 'a')
mass.write('Good luck with your studies!')
mass.close()

func(array)

# Напишите функцию считывающую данные из файла
def func(x):
    print(x)

mass = open('new_work', 'r')
print(mass.readline())
mass.close()

func(mass)


# Напишите функцию записи в файл которая приниммает в себя данные, отфильтровывает их и записывает
# - только отфильтрованные данные

array = [{
    'Toyota' : 'restyling',
    'age' : 2016,
    'Toyota' : 'not_restyling ',
    'age' : 2008
}]

def filter(x):
    for file in x:
        if file['age'] >= 2008:
            print('restyling')
        else:
            print('not restyling ')

mass = open('homework', 'w' )
mass.write('Vsem privet!')
mass.close()

filter(array)

