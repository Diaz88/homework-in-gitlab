# # Напишите функцию которая принемает *args
#
# def with_args(*args):
#     print(args)
# with_args('my_arr', 880,  'b', 'c' ,['d' , 'e'])
#
# # Напишите функцию которая принемает *kwargs
#
# def with_kwargs(**kwargs):
#     print(kwargs)
# with_kwargs(limon = 20, banan = 30, aple = 40, kakos = 60)
#
# # Напишите функцию которая принемает *args и проходит циклом по пришедшмим данным
# def with_args(*args):
#     for zikl in args:
#         print(zikl)
# with_args('my_arr', 880,  'b', 'c' ,['d' , 'e'])
#
#
#
# # Напишите имитацию работу клуба (массив девушек, массив парней, охраник, администратор), но с использованием параметров args, kwargs
#
girls = [
    {
        'name': 'Nazira',
        'age': 18},
    {
        'name': 'Elya',
        'age': 22},
    {
        'name': 'Gulima',
        'age': 15
    }
]

boys = [{
    'name' : 'Jusa',
    'age' : 25},
    {'name' : 'Tosya',
     'age' : 16},
    {'name' : 'Tyra',
     'age' : 19
}]

def club(*args, **kwargs):
    for i in args:
        for j in i:
            if j["age"] >= 18:
                print(f"{kwargs['secure']}: {j['name']} Добро пожаловать!")
                print(f"{kwargs['admin']}: Поставил печать {j['name']}\n")
            else:
                print(f"{kwargs['secure']}: {j['name']} вам нельзя пройти дальше!\n")


club(boys, girls, admin="Diaz", secure="Jack")